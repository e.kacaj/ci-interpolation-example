# CI Interpolation Example

This project shows how you define inputs for included CI/CD configurations, a new feature, [recently
introduced in 15.11](https://about.gitlab.com/releases/2023/04/22/gitlab-15-11-released/#define-inputs-for-included-cicd-configuration).

You can declare mandatory or optional input parameters for each includable configuration file
using `include` keyword in `.gitlab-ci.yml`.

In order to enable it, you need to define a template file with a leading
specification YAML document, that will describe what inputs arguments may be
used with the template. The template includes two sections, the input definition and the template content. we use three dash lines between the two parts:

```yaml
# deploy-template.yml

spec:
  inputs:
    website:
    environment:   
      default: test 
---    
deploy:
  stage: deploy
  script: echo "deploy $[[ inputs.website ]] to $[[inputs.enviornment]]"
```


In this template, we have two input parameters: `website` and `environment` . `environment` gets a default value. At the content section we define a job that uses the input arguemnts. 

Then you can include the file using various techniques that the [`include:`
keyword](https://docs.gitlab.com/ee/ci/yaml/#include) supports. We use the `inputs` keyword with the list of the input parameters:


```yaml
# .gitlab-ci.yml

include:
  - local: deploy-template.yml
    inputs:
      website: my-website.example.com

stages:
 - test
 - deploy

test:
  stage: test
  script: echo "test"
```
